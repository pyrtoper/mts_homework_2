package figures;

public class Circle extends GeomFigure {

    private Double centerX;
    private Double centerY;
    private Double radius;
    private String color;

    public Circle(Double centerX, Double centerY, Double radius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.color = "white";
    }

    public Circle(Double centerX, Double centerY, Double radius, String color) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.color = color;
    }

    @Override
    public Double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public Double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public Double getCenterX() {
        return centerX;
    }

    public void setCenterX(Double centerX) {
        this.centerX = centerX;
    }

    public Double getCenterY() {
        return centerY;
    }

    public void setCenterY(Double centerY) {
        this.centerY = centerY;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void colorCircle(String newColor) {
        this.color = newColor;
    }
}
