package figures;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Rhombus extends Polygon implements Equilateral {

    public Rhombus(List<List<Double>> vertexCoordinates) {
        super(vertexCoordinates);
        if (vertexCoordinates.size() != 4) {
            throw new IllegalArgumentException("Rhbmbus has 4 angles, but coordinates of " + vertexCoordinates.size() +
                    " were given");

        } else if (!checkEquilateralism()) {
            throw new IllegalArgumentException("Sides of Rhombus aren't equal, make sure that "
                + "coordinates were provided in a clockwise/counter clockwise order");
        }
    }

    @Override
    public boolean checkEquilateralism() {
        Set<BigDecimal> differentLengthSides = new HashSet<>();
        List<List<Double>> tempVertexCoordinates = new ArrayList<>(getVertexCoordinates());
        tempVertexCoordinates.add(getVertexCoordinates().get(0));
        for (int i = 0; i < tempVertexCoordinates.size() - 1; i++) {
            double sideLength = Math.sqrt(
                    Math.pow(tempVertexCoordinates.get(i + 1).get(0) - tempVertexCoordinates.get(i).get(0), 2) +
                            Math.pow(tempVertexCoordinates.get(i + 1).get(1) - tempVertexCoordinates.get(i).get(1), 2)
            );
            differentLengthSides.add(BigDecimal.valueOf(sideLength).setScale(5, RoundingMode.HALF_UP));
        }
        return differentLengthSides.size() == 1;
    }
}
