package figures;

public abstract class GeomFigure {

    public abstract Double getPerimeter();

    public abstract Double getArea();

}
