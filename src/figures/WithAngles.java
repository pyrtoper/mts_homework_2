package figures;

import java.util.List;

public interface WithAngles {

    List<List<Double>> getAngleVertexCoordinates();

}
