package figures;

import java.util.ArrayList;
import java.util.List;

public class Polygon extends GeomFigure implements WithAngles {

    private final Integer angleCount;

    private final List<List<Double>> vertexCoordinates;

    public Polygon(List<List<Double>> vertexCoordinates) {
        this.vertexCoordinates = vertexCoordinates;
        this.angleCount = this.vertexCoordinates.size();
    }

    @Override
    public List<List<Double>> getAngleVertexCoordinates() {
        return this.vertexCoordinates;
    }

    @Override
    public Double getPerimeter() {
        double result = 0.;
        List<List<Double>> tempVertexCoordinates = new ArrayList<>(vertexCoordinates);
        tempVertexCoordinates.add(vertexCoordinates.get(0));
        for (int i = 0; i < tempVertexCoordinates.size() - 1; i++) {
            result += Math.sqrt(
                Math.pow(tempVertexCoordinates.get(i + 1).get(0) - tempVertexCoordinates.get(i).get(0), 2) +
                Math.pow(tempVertexCoordinates.get(i + 1).get(1) - tempVertexCoordinates.get(i).get(1), 2)
            );
        }
        return result;
    }

    @Override
    public Double getArea() {
        double firstSum = 0.;
        double secondSum = 0;
        List<List<Double>> tempVertexCoordinates = new ArrayList<>(vertexCoordinates);
        tempVertexCoordinates.add(vertexCoordinates.get(0));
        for (int i = 0; i < tempVertexCoordinates.size() - 1; i++) {
            firstSum += tempVertexCoordinates.get(i).get(0) * tempVertexCoordinates.get(i + 1).get(1);
        }
        for (int i = 0; i < tempVertexCoordinates.size() - 1; i++) {
            secondSum += tempVertexCoordinates.get(i).get(1) * tempVertexCoordinates.get(i + 1).get(0);
        }
        return Math.abs(firstSum - secondSum) * 0.5;

    }

    public Integer getAngleCount() {
        return angleCount;
    }

    public List<List<Double>> getVertexCoordinates() {
        return vertexCoordinates;
    }
}
