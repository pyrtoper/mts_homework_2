package figures;

public class Ellipsis extends GeomFigure {

    private Double firstSemiAxis;
    private Double secondSemiAxis;
    private String color;

    public Ellipsis(Double firstSemiAxis, Double secondSemiAxis, String color) {
        this.firstSemiAxis = firstSemiAxis;
        this.secondSemiAxis = secondSemiAxis;
        this.color = color;
    }

    public Ellipsis(Double firstSemiAxis, Double secondSemiAxis) {
        this.firstSemiAxis = firstSemiAxis;
        this.secondSemiAxis = secondSemiAxis;
        this.color = "white";
    }

    @Override
    public Double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((Math.pow(firstSemiAxis, 2) + Math.pow(secondSemiAxis, 2)) / 8);
    }

    @Override
    public Double getArea() {
        return Math.PI * firstSemiAxis * secondSemiAxis;
    }

    public Double getFirstSemiAxis() {
        return firstSemiAxis;
    }

    public void setFirstSemiAxis(Double firstSemiAxis) {
        this.firstSemiAxis = firstSemiAxis;
    }

    public Double getSecondSemiAxis() {
        return secondSemiAxis;
    }

    public void setSecondSemiAxis(Double secondSemiAxis) {
        this.secondSemiAxis = secondSemiAxis;
    }

    public String getColor() {
        return color;
    }

    public void colorEllipsis(String newColor) {
        this.color = newColor;
    }
}
