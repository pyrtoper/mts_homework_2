import figures.Circle;
import figures.Ellipsis;
import figures.GeomFigure;
import figures.Rhombus;
import java.util.List;

public class Main {

  public static void main(String[] args) {

    Circle circle = new Circle(5., 4., 3.);

    System.out.println("***************************************");
    System.out.println("Color of Circle: " + circle.getColor());
    circle.colorCircle("red");
    System.out.println("Color of Circle: " + circle.getColor());
    System.out.println("CenterX of Circle: " + circle.getCenterX());
    System.out.println("CenterY of Circle: " + circle.getCenterY());
    System.out.println("Radius of Circle: " + circle.getRadius());
    System.out.println("Perimeter: " + circle.getPerimeter());
    System.out.println("Area: " + circle.getArea());

    Ellipsis ellipsis = new Ellipsis(5., 10.);

    System.out.println("***************************************");
    System.out.println("Color of Ellipsis: " + ellipsis.getColor());
    ellipsis.colorEllipsis("red");
    System.out.println("Color of Ellipsis: " + ellipsis.getColor());
    System.out.println("Perimeter: " + ellipsis.getPerimeter());
    System.out.println("Area: " + ellipsis.getArea());


    GeomFigure rhombus1 = new Rhombus(List.of(
        List.of(1., 3.),
        List.of(-0.5, 5.),
        List.of(-2., 3.),
        List.of(-0.5, 1.)
    ));

    System.out.println("*************************************");
    System.out.println("Area: " + rhombus1.getArea());
    System.out.println("Perimeter: " + rhombus1.getPerimeter());

    Rhombus rhombus2 = new Rhombus(List.of(
        List.of(2., 1.),
        List.of(4., 1.),
        List.of(4., -1.),
        List.of(2., -1.)
    ));

    System.out.println("****************************************");
    System.out.println("AngleCount: " + rhombus2.getAngleCount());
    System.out.println("AngleVertexCoordinates: " + rhombus2.getAngleVertexCoordinates());

    System.out.println("Sides are equal? " + rhombus2.checkEquilateralism());
    System.out.println("Perimeter: " + rhombus2.getPerimeter());
    System.out.println("Area: " + rhombus2.getArea());

  }
}
